import com.zuitt.example.Car;
import com.zuitt.example.Dog;

public class Main {
    public static void main(String[] args) {
//        System.out.println("Hello world!");

        Car myCar = new Car();
        myCar.drive();
        System.out.println(myCar.getDriverName());
        //System.out.println(myCar.d); res: cant access

        myCar.setBrand(("Kia"));
        myCar.setName("Sorrento");
        myCar.setYear_make(2022);
        System.out.println("The " + myCar.getBrand() + " "+ myCar.getName() + " "+ myCar.getYear_make() + " was driven by " + myCar.getDriverName());


        // another Car
        Car yourCar = new Car();
        System.out.println("Your car was driven by: " + yourCar.getDriverName());


        // Animal and Dog
        Dog myPet = new Dog();
        myPet.setName("Brownie");
        myPet.setColor("White");
        myPet.speak();

        System.out.println(myPet.getName() + " " + myPet.getColor() + " " + myPet.getBreed());


    }
}