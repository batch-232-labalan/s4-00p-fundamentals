package com.zuitt.example;

public class Dog extends Animal {
    // extends is a keyword used to inherit the properties of a class.
    private String breed;

    //constructors
    public Dog(){
        super(); // -> calls the Animal() constructor
        this.breed = "Golden Retriever";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }

    //getters and setters
    public String getBreed(){
        return this.breed;
    }

    public void setDogBreed(String dogBreed){
        this.breed = breed;
    }

    //method
    public void speak(){
        super.call();// calls the call() method from animal class
        System.out.println("Woof!");
    }

}
