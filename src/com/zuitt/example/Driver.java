package com.zuitt.example;

public class Driver {

    //property
    private String name;

    //parameterized constructor
    public Driver(String name){
        this.name = name;
    }

    //public constructor
    public Driver(){};

    //getter
    public String getName(){
        return this.name;
    }

    //setter
    public void setDriverName(String name){
        this.name = name;
    }

}

